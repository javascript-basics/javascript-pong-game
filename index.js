import {PongPresenter} from "./pong-game/pong_presenter.js";

class Starter {
    static #presenter;

    static start() {
        if (!this.#presenter) {
            this.#presenter = new PongPresenter();
        }
    }
}

Starter.start();
