import {PongElement} from "./pong_element.js";

const PLAYER_OFFSET_COEFFICIENT = 1.5;

const SPEED_COEFFICIENT = 4;
const COMPUTER_SPEED_COEFFICIENT = 1;

const TOP_POSITION_VALUE = 0;
const LEFT_POSITION_VALUE = 0;

const DEFAULT_SCORE_VALUE = 0;

export class PongService {
    #width;
    #height;

    #presenter;

    #human = new PongElement();
    #computer = new PongElement();
    #cube = new PongElement();

    #cubeXSpeed;
    #cubeYSpeed;

    #humanScore = DEFAULT_SCORE_VALUE;
    #computerScore = DEFAULT_SCORE_VALUE;

    get humanXPosition() {
        return this.#computer.xPosition;
    }

    get humanYPosition() {
        return this.#human.yPosition;
    }

    get computerXPosition() {
        return this.#computer.xPosition;
    }

    get computerYPosition() {
        return this.#computer.yPosition;
    }

    get cubeXPosition() {
        return this.#cube.xPosition;
    }

    get cubeYPosition() {
        return this.#cube.yPosition;
    }

    constructor(presenter, contentWidth, contentHeight, defaultRootValue) {
        this.#init(presenter, contentWidth, contentHeight, defaultRootValue);
    }

    #init(presenter, contentWidth, contentHeight, defaultRootValue) {
        this.#presenter = presenter;
        this.#width = contentWidth;
        this.#height = contentHeight;

        this.#initPositions(defaultRootValue);

        this.#cubeXSpeed = Math.sqrt(contentWidth) / SPEED_COEFFICIENT;
        this.#cubeYSpeed = Math.sqrt(contentHeight) / SPEED_COEFFICIENT;
    }

    #initPositions(defaultRootValue) {
        const playerPosition = PLAYER_OFFSET_COEFFICIENT * defaultRootValue;

        this.#human.xPosition = playerPosition;
        this.#human.yPosition = playerPosition;

        this.#computer.xPosition = playerPosition;
        this.#computer.yPosition = playerPosition;

        this.#setDefaultCubePosition();
    }

    calculateNextHumanPosition(direction) {
        const newYPosition = Math.round(this.#human.yPosition + direction *
            Math.round(this.#height / SPEED_COEFFICIENT / 2));
        const maxYPosition = Math.round(this.#height - this.#presenter.humanHeight);

        const topReached = newYPosition <= TOP_POSITION_VALUE;
        const bottomReached = newYPosition > maxYPosition;

        this.#human.yPosition = newYPosition;

        if (topReached) {
            this.#human.yPosition = TOP_POSITION_VALUE;
        } else if (bottomReached) {
            this.#human.yPosition = maxYPosition;
        }
    }

    calculateNextComputerPosition() {
        const positiveComputerYSpeed = Math.abs(this.#cubeYSpeed) * COMPUTER_SPEED_COEFFICIENT;

        const computerYSpeed = this.#cube.yPosition > this.#computer.yPosition ?
            positiveComputerYSpeed : -positiveComputerYSpeed;

        const newYPosition = Math.round(this.#computer.yPosition + computerYSpeed);
        const maxYPosition = Math.round(this.#height - this.#presenter.computerHeight - positiveComputerYSpeed);

        const topReached = newYPosition <= TOP_POSITION_VALUE;
        const bottomReached = newYPosition > maxYPosition;

        this.#computer.yPosition = newYPosition;

        if (topReached) {
            this.#computer.yPosition = TOP_POSITION_VALUE;
        } else if (bottomReached) {
            this.#computer.yPosition = maxYPosition;
        }
    }

    calculateNextCubePosition() {
        const humanWinCondition = this.#cube.xPosition + this.#presenter.cubeWidth >=
            this.#width - this.#computer.xPosition;

        if (humanWinCondition) {
            this.#humanScore++;
            this.#cubeXSpeed = -this.#cubeXSpeed;
        }

        const computerWinCondition = this.#cube.xPosition <= this.#human.xPosition;

        if (computerWinCondition) {
            this.#computerScore++;
            this.#cubeXSpeed = Math.abs(this.#cubeXSpeed);
        }

        const computerXPositionTouched = this.#width - this.#computer.xPosition - this.#presenter.computerWidth <
            this.#cube.xPosition + this.#presenter.cubeWidth;

        const computerTop = this.#cube.yPosition + this.#presenter.cubeHeight <=
            this.#computer.yPosition + this.#presenter.computerHeight;
        const computerBottom = this.#cube.yPosition + this.#presenter.cubeHeight >= this.#computer.yPosition;
        const computerYPositionTouched = computerTop && computerBottom;

        const humanXPositionTouched = this.#cube.xPosition - this.#presenter.cubeWidth <= this.#human.xPosition;

        const humanTop = this.#height - this.#cube.yPosition >= this.#human.yPosition;
        const humanBottom = this.#height - this.#cube.yPosition - this.#presenter.cubeHeight <=
            this.#human.yPosition + this.#presenter.humanHeight
        const humanYPositionTouched = humanTop && humanBottom;

        if (humanWinCondition || computerWinCondition) {
            this.#setDefaultCubePosition();
            this.#presenter.updateScores(this.#humanScore, this.#computerScore);
            this.#presenter.updateBackground();

            this.#cubeYSpeed = -this.#cubeYSpeed;
        }

        if (humanXPositionTouched && humanYPositionTouched) {
            this.#cubeXSpeed = Math.abs(this.#cubeXSpeed);
        }

        if (computerXPositionTouched && computerYPositionTouched) {
            this.#cubeXSpeed = -Math.abs(this.#cubeXSpeed);
        }

        const leftReached = this.#cube.xPosition < LEFT_POSITION_VALUE;
        const rightReached = this.#width - this.#presenter.cubeWidth - this.#cubeXSpeed < this.#cube.xPosition;

        if (leftReached || rightReached) {
            this.#cubeXSpeed = -this.#cubeXSpeed;
        }

        const topReached = this.#cube.yPosition < TOP_POSITION_VALUE;
        const bottomReached = this.#height - this.#presenter.cubeHeight - this.#cubeYSpeed < this.#cube.yPosition;

        if (bottomReached || topReached) {
            this.#cubeYSpeed = -this.#cubeYSpeed;
        }

        this.#cube.xPosition += this.#cubeXSpeed;
        this.#cube.yPosition += this.#cubeYSpeed;
    }

    #setDefaultCubePosition() {
        this.#cube.xPosition = this.#width / 2;
        this.#cube.yPosition = this.#height / 2;
    }

    updateContentWidth(newValue) {
        this.#width = newValue;
    }

    updateContentHeight(newValue) {
        this.#height = newValue;
    }

    updateCubeSpeed() {
        this.#cubeXSpeed = Math.sqrt(this.#width) / SPEED_COEFFICIENT;
        this.#cubeYSpeed = Math.sqrt(this.#height) / SPEED_COEFFICIENT;
    }
}
