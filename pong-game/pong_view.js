function getElementByClassName(className) {
    return (document.getElementsByClassName(className) || document.createDocumentFragment().children).item(0);
}

function getRandomColor() {
    const min = 0;
    const max = 255;

    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getLuma(red, green, blue) {
    const redCoefficient = 0.2126;
    const greenCoefficient = 0.7152;
    const blueCoefficient = 0.0722;

    return Math.sqrt(redCoefficient * red ** 2 + greenCoefficient * green ** 2 + blueCoefficient * blue ** 2);
}

const MAIN_CONTENT_CLASS = 'main-content';

const HUMAN_CLASS = 'human';
const COMPUTER_CLASS = 'computer';
const CUBE_CLASS = 'cube';

const HUMAN_SCORE_CLASS = 'human-score';
const COMPUTER_SCORE_CLASS = 'computer-score';

const MEDIAN_BYTE_VALUE = 255 / 2;
const WHITE_COLOR = '#ffffff';
const BLACK_COLOR = '#000000';

const DEFAULT_ROOT_VALUE_IN_PX = 16;

export class PongView {
    #mainContentElement = Object.create(HTMLElement.prototype, {});

    #humanElement = Object.create(HTMLElement.prototype, {});
    #computerElement = Object.create(HTMLElement.prototype, {});
    #cubeElement = Object.create(HTMLElement.prototype, {});

    #humanScoreElement = Object.create(HTMLElement.prototype, {});
    #computerScoreElement = Object.create(HTMLElement.prototype, {});

    get defaultRootValue() {
        return DEFAULT_ROOT_VALUE_IN_PX;
    }

    get contentWidth() {
        return this.#mainContentElement.getBoundingClientRect().width;
    }

    get contentHeight() {
        return this.#mainContentElement.getBoundingClientRect().height;
    }

    get humanHeight() {
        return this.#humanElement.getBoundingClientRect().height;
    }

    get computerWidth() {
        return this.#computerElement.getBoundingClientRect().width;
    }

    get computerHeight() {
        return this.#computerElement.getBoundingClientRect().height;
    }

    get cubeWidth() {
        return this.#cubeElement.getBoundingClientRect().width;
    }

    get cubeHeight() {
        return this.#cubeElement.getBoundingClientRect().height;
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#mainContentElement = getElementByClassName(MAIN_CONTENT_CLASS);

        this.#humanElement = getElementByClassName(HUMAN_CLASS);
        this.#computerElement = getElementByClassName(COMPUTER_CLASS);
        this.#cubeElement = getElementByClassName(CUBE_CLASS);

        this.#humanScoreElement = getElementByClassName(HUMAN_SCORE_CLASS);
        this.#computerScoreElement = getElementByClassName(COMPUTER_SCORE_CLASS);
    }

    setupRandomBackgroundColor() {
        const red = getRandomColor();
        const green = getRandomColor();
        const blue = getRandomColor();

        this.#mainContentElement.style.background = `rgb(${red}, ${green}, ${blue})`;

        const luma = getLuma(red, green, blue);

        const color = luma > MEDIAN_BYTE_VALUE ? WHITE_COLOR : BLACK_COLOR;

        this.#humanElement.style.background = color;
        this.#computerElement.style.background = color;
        this.#cubeElement.style.background = color;

        this.#humanScoreElement.style.color = color;
        this.#computerScoreElement.style.color = color;
    }

    updateHumanXPosition(value) {
        this.#humanElement.style.left = `${value}px`;
    }

    updateHumanYPosition(value) {
        this.#humanElement.style.top = `${value}px`;
    }

    updateComputerXPosition(value) {
        this.#computerElement.style.right = `${value}px`;
    }

    updateComputerYPosition(value) {
        this.#computerElement.style.bottom = `${value}px`;
    }

    updateCubeXPosition(value) {
        this.#cubeElement.style.left = `${value}px`;
    }

    updateCubeYPosition(value) {
        this.#cubeElement.style.bottom = `${value}px`;
    }

    updateHumanScore(value) {
        this.#humanScoreElement.innerText = value;
    }

    updateComputerScore(value) {
        this.#computerScoreElement.innerText = value;
    }
}
