import {PongView} from "./pong_view.js";
import {PongService} from "./pong_service.js";

const ARROW_UP_KEY = 'ArrowUp';
const ARROW_DOWN_KEY = 'ArrowDown';

const UP_DIRECTION = -1;
const DOWN_DIRECTION = 1;

export class PongPresenter {
    #pongView = new PongView();
    #pongService;

    get humanHeight() {
        return this.#pongView.humanHeight;
    }

    get computerWidth() {
        return this.#pongView.computerWidth;
    }

    get computerHeight() {
        return this.#pongView.computerHeight;
    }

    get cubeWidth() {
        return this.#pongView.cubeWidth;
    }

    get cubeHeight() {
        return this.#pongView.cubeHeight;
    }

    constructor() {
        this.#init();
    }

    #init() {
        this.#pongService = new PongService(this, this.#pongView.contentWidth, this.#pongView.contentHeight,
            this.#pongView.defaultRootValue);

        this.#pongView.setupRandomBackgroundColor();
        this.#initListeners();
        this.#animate();
    }

    #initListeners() {
        document.addEventListener('keydown', this.#handleKey.bind(this));
        document.addEventListener('resize', this.#handleResize.bind(this));
    }

    #handleKey({code}) {
        if (code === ARROW_UP_KEY) {
            this.#pongService.calculateNextHumanPosition(UP_DIRECTION);
        }

        if (code === ARROW_DOWN_KEY) {
            this.#pongService.calculateNextHumanPosition(DOWN_DIRECTION);
        }
    }

    #handleResize() {
        this.#pongService.updateContentWidth(this.#pongView.contentWidth);
        this.#pongService.updateContentHeight(this.#pongView.contentHeight);
        this.#pongService.updateCubeSpeed();
    }

    #animate() {
        this.#pongService.calculateNextComputerPosition();
        this.#pongService.calculateNextCubePosition();

        this.#updatePositions();

        requestAnimationFrame(this.#animate.bind(this));
    }

    #updatePositions() {
        this.#pongView.updateHumanXPosition(this.#pongService.humanXPosition);
        this.#pongView.updateHumanYPosition(this.#pongService.humanYPosition);

        this.#pongView.updateComputerXPosition(this.#pongService.computerXPosition);
        this.#pongView.updateComputerYPosition(this.#pongService.computerYPosition);

        this.#pongView.updateCubeXPosition(this.#pongService.cubeXPosition);
        this.#pongView.updateCubeYPosition(this.#pongService.cubeYPosition);
    }

    updateScores(humanScore, computerScore) {
        this.#pongView.updateHumanScore(humanScore);
        this.#pongView.updateComputerScore(computerScore);
    }

    updateBackground() {
        this.#pongView.setupRandomBackgroundColor();
    }
}
