export class PongElement {
    #xPosition;
    #yPosition;

    get xPosition() {
        return this.#xPosition;
    }

    set xPosition(value) {
        this.#xPosition = value;
    }

    get yPosition() {
        return this.#yPosition;
    }

    set yPosition(value) {
        this.#yPosition = value;
    }
}
